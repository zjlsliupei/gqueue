module gitee.com/zjlsliupei/gqueue

go 1.16

require (
	gitee.com/zjlsliupei/ghelp v0.0.14 // indirect
	github.com/beego/beego/v2 v2.0.1
	github.com/go-redis/redis/v8 v8.11.4
	github.com/tidwall/gjson v1.10.0
)
