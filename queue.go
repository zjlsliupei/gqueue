package gqueue

import (
	"fmt"
	"github.com/beego/beego/v2/core/logs"
)

type QueueDrive string

const (
	RedisDrive QueueDrive = "redis"
)

var (
	globalQueue IQueue
)

type IQueue interface {
	// Publish 发布消息，返回消息id
	Publish(queueName, msg string) (string, error)
	// Subscribe 订阅消息，成功后执行回调函数，
	// 回调返回true会自动ack，返回false不会ack
	Subscribe(queueName string, cb func(msg string) bool)
}

// QueueFactory 实例化消息队列
func QueueFactory(queueDrive QueueDrive, option string) (IQueue, error) {
	if queueDrive == RedisDrive {
		return NewQueueRedis(option)
	} else {
		return nil, fmt.Errorf("%s is not support", queueDrive)
	}
}

// InitGlobalQueue 初始化全局队列
func InitGlobalQueue(queueDrive QueueDrive, option string) error {
	var err error
	globalQueue, err = QueueFactory(queueDrive, option)
	if err != nil {
		logs.Error("InitGlobalQueue err", err)
	}
	return err
}

// GetGlobalQueue 返回全局队列
func GetGlobalQueue() IQueue {
	return globalQueue
}
