package gqueue

import (
	"fmt"
	"os"
	"testing"
)

func init() {
	conn := os.Getenv("TEST_REDIS")
	fmt.Println(conn)
	InitGlobalQueue(RedisDrive, conn)
}

func TestQueueRedis_Publish(t *testing.T) {

	q := GetGlobalQueue()
	q.Publish("testqueue", `{"aaa":"bbb"}`)
	q.Publish("testqueue2", `{"aaa":"bbb2"}`)
}

func TestQueueRedis_Subscribe(t *testing.T) {
	q := GetGlobalQueue()
	q.Subscribe("testqueue", func(msg string) bool {
		fmt.Println("testqueue", msg)
		return true
	})

	q.Subscribe("testqueue2", func(msg string) bool {
		fmt.Println("testqueue2", msg)
		return true
	})
	select {}
}
