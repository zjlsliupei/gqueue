# 消息队列
基于驱动开发的消息队列，目前支持redis驱动

## 安装
```go
go get gitee.com/zjlsliupei/gqueue
```

## 快速使用
消息队列初始化
```go
// 初始化全局消息队列，参数传入：驱动类型，驱动连接参数
InitGlobalQueue(RedisDrive, conn)
```
驱动类型参考
- RedisDrive：redis作为消息队列驱动，对应conn连接格式如：{"addr":"127.0.0.1:6379","password":"1234","db":0}

发布消息
```go
// 获取全局消息队列句柄
q := GetGlobalQueue()
// 发布消息，参数是：消息列队名、消息内容
q.Publish("testqueue", `{"aaa":"bbb"}`)
```

订单消息
```go
// 获取全局消息队列句柄
q := GetGlobalQueue()
q.Subscribe("testqueue", func(msg string) bool {
    fmt.Println("testqueue", msg)
    return true
})
```

